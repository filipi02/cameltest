package com.camel.test.java;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cameltest.java.MyBean;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyBean.class)
public class CamelTestApplicationTests {

	@Test
	public void contextLoads() {
	}

}
