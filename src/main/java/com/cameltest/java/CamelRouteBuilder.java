package com.cameltest.java;

import javax.ws.rs.core.MediaType;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class CamelRouteBuilder extends RouteBuilder {

	@Value("${server.port}")
	String serverPort;

	@Value("${rest.api.path}")
	String contextPath;

	@Bean
	ServletRegistrationBean servletRegistrationBean() {
		ServletRegistrationBean servlet = new ServletRegistrationBean(new CamelHttpTransportServlet(),
				contextPath + "/*");
		servlet.setName("CamelServlet");
		return servlet;
	}

	@Override
	public void configure() throws Exception {
		restConfiguration().contextPath(contextPath) //
				.port(serverPort).enableCORS(true).apiContextPath("/api-doc").apiProperty("api.title", "Test REST API")
				.apiProperty("api.version", "v1").apiProperty("cors", "true") // cross-site
				.apiContextRouteId("doc-api").component("servlet").bindingMode(RestBindingMode.json)
				.dataFormatProperty("prettyPrint", "true");

		/**
		 * Mapeamento rest.
		 */

		rest("/api/").description("Teste REST Service").id("api-route").post("/bean")
				.produces(MediaType.APPLICATION_JSON).consumes(MediaType.APPLICATION_JSON)
				.bindingMode(RestBindingMode.auto).type(MyBean.class).enableCORS(true).to("direct:remoteService");

		from("direct:remoteService").routeId("direct-route").tracing().log(">>> ${body.id}").log(">>> ${body.name}")
				.process(new Processor() {
					@Override
					public void process(Exchange exchange) throws Exception {
						MyBean bodyIn = (MyBean) exchange.getIn().getBody();

						ExampleServices.example(bodyIn);

						exchange.getIn().setBody(bodyIn);
					}
				}).setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201));
	}

}
