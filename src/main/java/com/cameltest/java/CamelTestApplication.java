package com.cameltest.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.cameltest.java")
public class CamelTestApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(CamelTestApplication.class, args);
	}
		
}
